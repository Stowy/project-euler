﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MaximumPathSumI
{
    internal static class Program
    {
        private static async Task Main(string[] args)
        {
            string filePath = args.Length > 0 ? args[0] : @"./triangle.txt";
            if (!File.Exists(filePath))
            {
                throw new ArgumentException($"Provided file : {filePath} doesn't exists.");
            }

            List<List<int>> triangle = await ReadTriangleFile(filePath);

            List<List<int>> nodesSum = new List<List<int>>();

            // Iterate through each line
            foreach (List<int> line in triangle)
            {
                // Iterate through each node
                for (int i = 0; i < line.Count; i++)
                {
                    List<List<int>> newNodesSum = new List<List<int>>();

                    // Get the node
                    int node = line[i];

                    // Iterate on the preceding nodes
                    for (int j = i - 1; j <= i; j++)
                    {
                        if (j < 0 || j >= nodesSum.Count) continue;
                        
                        // Iterate on the paths that the previous node has
                        foreach (int oldNode in nodesSum[j])
                        {
                            // Add the sum
                            newNodesSum[j].Add(node + oldNode);
                            
                        }
                    }

                    nodesSum = newNodesSum;
                }
            }

            // Put the sums in one list
            List<int> sums = nodesSum.SelectMany(list => list).ToList();

            // Sort the sums
            sums.Sort();

            // Console.WriteLine(sums.Count);
            // Print the biggest sum
            Console.WriteLine($"Biggest sum is : {sums[0]}");
        }

        private static async Task<List<List<int>>> ReadTriangleFile(string path)
        {
            Console.WriteLine("Read file...");
            List<List<int>> triangle = new List<List<int>>();
            using (StreamReader sr = File.OpenText(path))
            {
                string text = await sr.ReadToEndAsync();
                string[] lines = text.Split('\n');
                triangle.AddRange(lines.Select(line => line.Split(' '))
                    .Select(numbers => numbers.Select(number => Convert.ToInt32(number)).ToList()));
            }

            return triangle;
        }
    }
}