﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MultiplesOf3And5
{
    internal static class Program
    {
        private const int MaxValue = 1000;

        private static void Main()
        {
            List<int> multiples = new List<int>();
            for (int i = 0; i < MaxValue; i++)
            {
                if (i % 3 == 0 || i % 5 == 0)
                {
                    multiples.Add(i);
                }
            }

            int sum = multiples.Sum();

            Console.WriteLine(sum);
        }
    }
}